cmake_minimum_required(VERSION 3.5)

set(KTACTILE_FEEDBACK_VERSION "1.0.0") # handled by release scripts
project(KTactileFeedback VERSION ${KTACTILE_FEEDBACK_VERSION})

# ECM setup
include(FeatureSummary)
find_package(ECM "6.3.0" NO_MODULE)
set_package_properties(ECM PROPERTIES TYPE REQUIRED DESCRIPTION "Extra CMake Modules." URL "https://commits.kde.org/extra-cmake-modules")
feature_summary(WHAT REQUIRED_PACKAGES_NOT_FOUND FATAL_ON_MISSING_REQUIRED_PACKAGES)

set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDEGitCommitHooks)
include(KDECompilerSettings NO_POLICY_SCOPE)

include(ECMGenerateExportHeader)
include(CMakePackageConfigHelpers)
include(ECMSetupVersion)
include(ECMGenerateHeaders)
include(ECMAddQch)
include(ECMAddTests)
include(ECMQtDeclareLoggingCategory)

# Dependencies
set(REQUIRED_QT_VERSION 6.4.0)

find_package(Qt6 ${REQUIRED_QT_VERSION} REQUIRED CONFIG COMPONENTS Core Qml Multimedia)

set(EXCLUDE_DEPRECATED_BEFORE_AND_AT 0 CACHE STRING "Control the range of deprecated API excluded from the build [default=0].")

option(BUILD_QCH "Build API documentation in QCH format (for e.g. Qt Assistant, Qt Creator & KDevelop)" OFF)
add_feature_info(QCH ${BUILD_QCH} "API documentation in QCH format (for e.g. Qt Assistant, Qt Creator & KDevelop)")
option(ENABLE_QTMULTIMEDIA_BACKEND "Build the QtMultimedia backend" ON)
option(BUILD_TESTING "Build tests" ON)

add_definitions(-DQT_DISABLE_DEPRECATED_BEFORE=0x050f00)
add_definitions(-DQT_NO_FOREACH)

add_subdirectory(src)
if (BUILD_TESTING)
    add_subdirectory(autotests)
endif()

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
