// SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
//
// SPDX-License-Identifier: BSD-3-Clause

#include "hapticsquare.h"
#include "hapticbutton.h"

#include <QGridLayout>
#include <QDebug>

//! [0]
HapticSquare::HapticSquare()
{
    m_rumble.setAttackIntensity(0.1);
    m_rumble.setAttackTime(250);
    m_rumble.setIntensity(1.0);
    m_rumble.setDuration(1000);
    m_rumble.setFadeTime(250);
    m_rumble.setFadeIntensity(0.1);
//! [0]

//! [1]
    m_ocean.setAttackIntensity(0.1);
    m_ocean.setAttackTime(450);
    m_ocean.setIntensity(0.8);
    m_ocean.setDuration(6000);
    m_ocean.setFadeTime(900);
    m_ocean.setFadeIntensity(0.05);
    m_ocean.setPeriod(1500);
//! [1]

//! [2]
    m_btnRumble = new HapticButton(tr("Rumble!"));
    m_btnOcean = new HapticButton(tr("Ocean"));
    m_btnButtonClick = new HapticButton(tr("Click"));
    m_btnNegativeEffect = new HapticButton(tr("Oops!"));
    QGridLayout *topLayout = new QGridLayout(this);
    topLayout->addWidget(m_btnRumble, 0, 0);
    topLayout->addWidget(m_btnOcean, 0, 1);
    topLayout->addWidget(m_btnButtonClick, 1, 0);
    topLayout->addWidget(m_btnNegativeEffect, 1, 1);

    connect(m_btnRumble, SIGNAL(clicked()), this, SLOT(playRumble()));
    connect(m_btnOcean, SIGNAL(clicked()), this, SLOT(playOcean()));
    connect(m_btnButtonClick, SIGNAL(clicked()), this, SLOT(playButtonClick()));
    connect(m_btnNegativeEffect, SIGNAL(clicked()), this, SLOT(playNegativeEffect()));
}
//! [2]

HapticSquare::~HapticSquare()
{
    delete m_btnRumble;
    delete m_btnOcean;
    delete m_btnButtonClick;
    delete m_btnNegativeEffect;
}

//! [3]
void HapticSquare::playRumble()
{
    m_rumble.start();
}

void HapticSquare::playOcean()
{
    if (m_ocean.state() == QFeedbackEffect::Stopped) {
        m_ocean.start();
    } else {
        m_ocean.stop();
    }
}
//! [3]

//! [4]
void HapticSquare::playButtonClick()
{
    QFeedbackEffect::playThemeEffect(QFeedbackEffect::ThemeBasicButton);
}

void HapticSquare::playNegativeEffect()
{
    QFeedbackEffect::playThemeEffect(QFeedbackEffect::ThemeNegativeTacticon);
}
//! [4]

#include "moc_hapticsquare.cpp"
