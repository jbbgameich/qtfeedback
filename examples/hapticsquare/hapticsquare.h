// SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
//
// SPDX-License-Identifier: BSD-3-Clause

#include <qfeedbackeffect.h>

#include <QWidget>
class HapticButton;

#ifndef HAPTICSQUARE_H_
#define HAPTICSQUARE_H_

//! [0]
class HapticSquare : public QWidget
{
    Q_OBJECT

public:
    HapticSquare();
    ~HapticSquare();

private Q_SLOTS:
    void playRumble();
    void playOcean();
    void playButtonClick();
    void playNegativeEffect();

private:
    HapticButton *m_btnRumble;
    HapticButton *m_btnOcean;
    HapticButton *m_btnButtonClick;
    HapticButton *m_btnNegativeEffect;

    QFeedbackHapticsEffect m_rumble;
    QFeedbackHapticsEffect m_ocean;
};
//! [0]

#endif

