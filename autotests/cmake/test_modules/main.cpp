/****************************************************************************
**
** SPDX-FileCopyrightText: 2012 Klarälvdalens Datakonsult AB a KDAB Group company, info@kdab.com, author Stephen Kelly <stephen.kelly@kdab.com>
** SPDX-FileCopyrightText: 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL-EXCEPT$
** SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0 OR LicenseRef-Qt-Commercial
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QFeedbackHapticsEffect>

int main(int argc, char **argv)
{
    QFeedbackHapticsEffect feedbackHapticsEffect;

    return 0;
}
