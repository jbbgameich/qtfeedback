/****************************************************************************
**
** SPDX-FileCopyrightText: 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL-EXCEPT$
** SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0 OR LicenseRef-Qt-Commercial
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QFEEDBACKTESTPLUGIN_H
#define QFEEDBACKTESTPLUGIN_H

#include <QtCore/QList>
#include <QtCore/QVector>
#include <QtCore/QHash>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QMutex>
#include <QtCore/QTimer>

#include <qfeedbackplugininterfaces.h>

QT_BEGIN_HEADER
QT_USE_NAMESPACE

class QFeedbackTestPlugin : public QObject, public QFeedbackThemeInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtFeedbackTestPlugin2")
    Q_INTERFACES(QFeedbackThemeInterface)
public:
    QFeedbackTestPlugin();
    virtual ~QFeedbackTestPlugin();

    virtual PluginPriority pluginPriority() {return QFeedbackInterface::PluginNormalPriority;}
    virtual bool play(QFeedbackEffect::Effect) {return false;}
};


QT_END_HEADER

#endif
