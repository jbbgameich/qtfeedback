/****************************************************************************
**
** SPDX-FileCopyrightText: 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL-EXCEPT$
** SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0 OR LicenseRef-Qt-Commercial
**
** $QT_END_LICENSE$
**
****************************************************************************/

import org.kde.tactilefeedback

FileEffect {
    loaded: false
    source: "qrc:nonexistingfile.haptic"
}
