/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Feedback framework.  This header file may change from version
// to version without notice, or even be removed.
//
// We mean it.
//
//

#ifndef QFEEDBACKPLUGIN_P_H
#define QFEEDBACKPLUGIN_P_H

#include "qfeedbackactuator.h"
#include "qfeedbackeffect.h"
#include "qfeedbackplugininterfaces.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QVariant>

QT_BEGIN_NAMESPACE

class QDummyBackend : QObject, public QFeedbackHapticsInterface
{
public:
    QDummyBackend() : QObject(qApp) { pluginPriority(); }

    QList<QFeedbackActuator*> actuators() override { return QList<QFeedbackActuator*>(); }

    void setActuatorProperty(const QFeedbackActuator &, ActuatorProperty, const QVariant &) override { }
    QVariant actuatorProperty(const QFeedbackActuator &, ActuatorProperty) override { return QVariant(); }
    bool isActuatorCapabilitySupported(const QFeedbackActuator &, QFeedbackActuator::Capability) override { return false; }

    void updateEffectProperty(const QFeedbackHapticsEffect *, EffectProperty) override { }
    void setEffectState(const QFeedbackHapticsEffect *, QFeedbackEffect::State) override { }
    QFeedbackEffect::State effectState(const QFeedbackHapticsEffect *) override { return QFeedbackEffect::Stopped; }

    PluginPriority pluginPriority() override { return PluginLowPriority; }
};

QT_END_NAMESPACE

#endif // QFEEDBACKPLUGIN_P_H
