/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Feedback framework.  This header file may change from version
// to version without notice, or even be removed.
//
// We mean it.
//
//

#ifndef QFEEDBACKEFFECT_P_H
#define QFEEDBACKEFFECT_P_H

#include <qfeedbackeffect.h>
#include <qfeedbackactuator.h>

QT_BEGIN_NAMESPACE

class QFeedbackHapticsEffectPrivate
{
public:
    QFeedbackHapticsEffectPrivate()
        : duration(250)
        , attackTime(0)
        , fadeTime(0)
        , period(-1)
        , actuator(nullptr)
        , intensity(1)
        , attackIntensity(0)
        , fadeIntensity(0)
    {

    }

    // Try to avoid holes (mostly where qreal == double)
    int duration;
    int attackTime;
    int fadeTime;
    int period;
    QFeedbackActuator *actuator;
    qreal intensity;
    qreal attackIntensity;
    qreal fadeIntensity;
};

class QFeedbackFileEffectPrivate
{
public:
    QFeedbackFileEffectPrivate(QFeedbackFileEffect *effect)
        : effect(effect)
        , loaded(false)
        , backendUsed(-1)
    {
    }

    static QFeedbackFileEffectPrivate *get(QFeedbackFileEffect *e) { return e->priv.get(); }
    static const QFeedbackFileEffectPrivate *get(const QFeedbackFileEffect *e) { return e->priv.get(); }

    void loadFinished(bool success);

    QFeedbackFileEffect *effect;

    QUrl url;
    bool loaded;

    //used for loading the file
    int backendUsed;
};

QT_END_NAMESPACE

#endif // QFEEDBACKEFFECT_P_H
