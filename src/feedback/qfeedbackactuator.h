/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

#ifndef QFEEDBACKACTUATOR_H
#define QFEEDBACKACTUATOR_H

#include "qfeedbackglobal.h"
#include <QtCore/QObject>

class QFeedbackEffect;

class Q_FEEDBACK_EXPORT QFeedbackActuator : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QFeedbackActuator::State state READ state)
    Q_PROPERTY(bool valid READ isValid CONSTANT)
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)

public:
    enum Capability {
        Envelope,
        Period
    };

    enum State {
        Busy,
        Ready,
        Unknown
    };

    Q_ENUM(Capability)
    Q_ENUM(State)

    explicit QFeedbackActuator(QObject *parent = nullptr);

    int id() const;
    bool isValid() const;

    QString name() const;
    State state() const;

    Q_INVOKABLE bool isCapabilitySupported(Capability) const;

    bool isEnabled() const;
    void setEnabled(bool);

    static QList<QFeedbackActuator*> actuators();
    bool operator==(const QFeedbackActuator&) const;

Q_SIGNALS:
    void enabledChanged();

private:
    QFeedbackActuator(QObject *parent, int id);
    friend class QFeedbackHapticsInterface;
    int m_id;
};

#endif // QFEEDBACKACTUATOR_H
