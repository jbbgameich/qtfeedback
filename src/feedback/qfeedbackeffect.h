/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

#ifndef QFEEDBACKEFFECT_H
#define QFEEDBACKEFFECT_H

#include "qfeedbackglobal.h"

#include <QtCore/QObject>
#include <QtCore/QStringList>
#include <QtCore/QUrl>

#include <memory>

class QFeedbackActuator;
class QFeedbackFileEffectPrivate;
class QFeedbackHapticsEffectPrivate;

class Q_FEEDBACK_EXPORT QFeedbackEffect : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int duration READ duration)
    Q_PROPERTY(State state READ state NOTIFY stateChanged)

public:
    // Make sure these are kept up to date with the declarative version
    enum Effect {
        Undefined = -1, Press, Release, PressWeak, ReleaseWeak, PressStrong, ReleaseStrong, DragStart,
        DragDropInZone, DragDropOutOfZone, DragCrossBoundary, Appear, Disappear, Move,
        NumberOfEffects,
        UserEffect = 65535
    };

    enum Duration {
        Infinite = -1
    };

    enum State {
        Stopped,
        Paused,
        Running,
        Loading
    };

    enum ErrorType {
        UnknownError,
        DeviceBusy
    };

    Q_ENUM(Effect)
    Q_ENUM(Duration)
    Q_ENUM(State)
    Q_ENUM(ErrorType)

    explicit QFeedbackEffect(QObject *parent = nullptr);

    virtual State state() const = 0;
    virtual int duration() const = 0;

    //for themes
    static bool supportsThemeEffect();
    static bool playThemeEffect(Effect effect);

public Q_SLOTS:
    void start();
    void stop();
    void pause();

protected:
    virtual void setState(State) = 0;

Q_SIGNALS:
    void error(QFeedbackEffect::ErrorType) const; //when an error occurs
    void stateChanged();

private:
    friend class QFeedbackInterface;
};

class Q_FEEDBACK_EXPORT QFeedbackHapticsEffect : public QFeedbackEffect
{
    Q_OBJECT

    Q_PROPERTY(int duration READ duration WRITE setDuration NOTIFY durationChanged)
    Q_PROPERTY(qreal intensity READ intensity WRITE setIntensity NOTIFY intensityChanged)
    Q_PROPERTY(int attackTime READ attackTime WRITE setAttackTime NOTIFY attackTimeChanged)
    Q_PROPERTY(qreal attackIntensity READ attackIntensity WRITE setAttackIntensity NOTIFY attackIntensityChanged)
    Q_PROPERTY(int fadeTime READ fadeTime WRITE setFadeTime NOTIFY fadeTimeChanged)
    Q_PROPERTY(qreal fadeIntensity READ fadeIntensity WRITE setFadeIntensity NOTIFY fadeIntensityChanged)
    Q_PROPERTY(int period READ period WRITE setPeriod NOTIFY periodChanged)
    Q_PROPERTY(QFeedbackActuator* actuator READ actuator WRITE setActuator NOTIFY actuatorChanged)

public:
    explicit QFeedbackHapticsEffect(QObject *parent = nullptr);
    ~QFeedbackHapticsEffect() override;

    void setDuration(int msecs);
    int duration() const override;

    void setIntensity(qreal intensity);
    qreal intensity() const;

    //the envelope
    void setAttackTime(int msecs);
    int attackTime() const;

    void setAttackIntensity(qreal intensity);
    qreal attackIntensity() const;

    void setFadeTime(int msecs);
    int fadeTime() const;

    void setFadeIntensity(qreal intensity);
    qreal fadeIntensity() const;

    void setPeriod(int msecs);
    int period() const;

    void setActuator(QFeedbackActuator *actuator);
    QFeedbackActuator* actuator() const;

    //reimplementations from QFeedbackEffect
    State state() const override;

Q_SIGNALS:
    void durationChanged();
    void intensityChanged();
    void attackTimeChanged();
    void attackIntensityChanged();
    void fadeTimeChanged();
    void fadeIntensityChanged();
    void periodChanged();
    void actuatorChanged();

protected:
    void setState(State) override;

private:
    Q_DISABLE_COPY(QFeedbackHapticsEffect)
    friend class QFeedbackHapticsEffectPrivate;
    std::unique_ptr<QFeedbackHapticsEffectPrivate> priv;
};

class Q_FEEDBACK_EXPORT QFeedbackFileEffect : public QFeedbackEffect
{
    Q_OBJECT

    Q_PROPERTY(bool loaded READ isLoaded WRITE setLoaded NOTIFY loadedChanged)
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)

public:
    explicit QFeedbackFileEffect(QObject *parent = nullptr);
    ~QFeedbackFileEffect() override;

    int duration() const override;

    bool isLoaded() const;

    void load();
    void unload();
    void setLoaded(bool);

    QUrl source() const;
    void setSource(const QUrl &);

    static QStringList supportedMimeTypes();

    //reimplementations from QFeedbackEffect
    State state() const override;

Q_SIGNALS:
    void loadedChanged();
    void sourceChanged();

protected:
    void setState(State) override;

private:
    Q_DISABLE_COPY(QFeedbackFileEffect)
    friend class QFeedbackFileEffectPrivate;
    std::unique_ptr<QFeedbackFileEffectPrivate> priv;
};

#endif // QFEEDBACKEFFECT_H
