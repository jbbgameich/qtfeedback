/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

#ifndef QFEEDBACKGLOBAL_H
#define QFEEDBACKGLOBAL_H

#include <QtCore/qglobal.h>

#ifndef QT_STATIC
#  if defined(QT_BUILD_FEEDBACK_LIB)
#    define Q_FEEDBACK_EXPORT Q_DECL_EXPORT
#  else
#    define Q_FEEDBACK_EXPORT Q_DECL_IMPORT
#  endif
#else
#  define Q_FEEDBACK_EXPORT
#endif

#endif // QFEEDBACKGLOBAL_H

