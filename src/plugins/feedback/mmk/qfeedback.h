/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

#ifndef QFEEDBACK_MMK_H
#define QFEEDBACK_MMK_H

#include <QtCore/QHash>

#include <qfeedbackplugininterfaces.h>

#include <QSoundEffect>

class QFeedbackMediaObject;

class QFeedbackMMK : public QObject, public QFeedbackFileInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtFeedbackPlugin" FILE "mmk.json")
    Q_INTERFACES(QFeedbackFileInterface)

public:
    QFeedbackMMK();
    ~QFeedbackMMK() override;

    void setLoaded(QFeedbackFileEffect*, bool) override;
    void setEffectState(QFeedbackFileEffect *, QFeedbackEffect::State) override;
    QFeedbackEffect::State effectState(const QFeedbackFileEffect *) override;
    int effectDuration(const QFeedbackFileEffect*) override;
    QStringList supportedMimeTypes() override;

private Q_SLOTS:
    void soundEffectStatusChanged();
    void soundEffectPlayingChanged();

private:
    struct FeedbackInfo {
        FeedbackInfo() : soundEffect(nullptr), loaded(false), playing(false) {}
        QSoundEffect* soundEffect;
        bool loaded;
        bool playing;
    };

    QHash<const QFeedbackFileEffect*, FeedbackInfo> mEffects;
    QHash<const QSoundEffect*, QFeedbackFileEffect*> mEffectMap;
};

#endif // QFEEDBACK_MMK_H
