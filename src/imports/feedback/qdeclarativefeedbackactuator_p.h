/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Feedback framework.  This header file may change from version
// to version without notice, or even be removed.
//
// We mean it.
//
//

#ifndef QDECLARATIVEFEEDBACKACTUATOR_P_H
#define QDECLARATIVEFEEDBACKACTUATOR_P_H

#include <QtQml/qqml.h>
#include "qfeedbackactuator.h"

QT_USE_NAMESPACE

class QDeclarativeFeedbackActuator : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int actuatorId READ actuatorId CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(State state READ state CONSTANT)
    Q_PROPERTY(bool valid READ isValid CONSTANT)
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)

public:
    enum Capability {
        Envelope = QFeedbackActuator::Envelope,
        Period = QFeedbackActuator::Period
    };

    enum State {
        Busy = QFeedbackActuator::Busy,
        Ready = QFeedbackActuator::Ready,
        Unknown = QFeedbackActuator::Unknown
    };

    Q_ENUM(Capability)
    Q_ENUM(State)

    explicit QDeclarativeFeedbackActuator(QObject *parent = nullptr);
    explicit QDeclarativeFeedbackActuator(QObject *parent, QFeedbackActuator* actuator);
    QFeedbackActuator* feedbackActuator() const;
    int actuatorId() const;
    bool isValid() const;
    QString name() const;
    State state() const;
    Q_INVOKABLE bool isCapabilitySupported(Capability capability) const;
    bool isEnabled() const;
    void setEnabled(bool v);

Q_SIGNALS:
    void enabledChanged();

private:
    QFeedbackActuator* d;
};

QML_DECLARE_TYPE(QDeclarativeFeedbackActuator)

#endif // QDECLARATIVEFEEDBACKACTUATOR_P_H
