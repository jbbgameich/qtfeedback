/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Feedback framework.  This header file may change from version
// to version without notice, or even be removed.
//
// We mean it.
//
//

#ifndef QDECLARATIVEHAPTICSEFFECT_P_H
#define QDECLARATIVEHAPTICSEFFECT_P_H

#include "qdeclarativefeedbackeffect_p.h"
#include "qdeclarativefeedbackactuator_p.h"

QT_USE_NAMESPACE

class QDeclarativeHapticsEffect : public QDeclarativeFeedbackEffect
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<QDeclarativeFeedbackActuator> availableActuators READ availableActuators)
    Q_PROPERTY(qreal intensity READ intensity WRITE setIntensity NOTIFY intensityChanged)
    Q_PROPERTY(int attackTime READ attackTime WRITE setAttackTime NOTIFY attackTimeChanged)
    Q_PROPERTY(qreal attackIntensity READ attackIntensity WRITE setAttackIntensity NOTIFY attackIntensityChanged)
    Q_PROPERTY(int fadeTime READ fadeTime WRITE setFadeTime NOTIFY fadeTimeChanged)
    Q_PROPERTY(qreal fadeIntensity READ fadeIntensity WRITE setFadeIntensity NOTIFY fadeIntensityChanged)
    Q_PROPERTY(int period READ period WRITE setPeriod NOTIFY periodChanged)
    Q_PROPERTY(QDeclarativeFeedbackActuator* actuator READ actuator WRITE setActuator NOTIFY actuatorChanged)

public:
    explicit QDeclarativeHapticsEffect(QObject *parent = nullptr);

    void setDuration(int msecs) override ;
    int duration() const override ;
    void setIntensity(qreal intensity);
    qreal intensity() const;
    //the envelope
    void setAttackTime(int msecs);
    int attackTime() const;
    void setAttackIntensity(qreal intensity);
    qreal attackIntensity() const;
    void setFadeTime(int msecs);
    int fadeTime() const;
    void setFadeIntensity(qreal intensity);
    qreal fadeIntensity() const;
    void setPeriod(int msecs);
    int period() const;
    void setActuator(QDeclarativeFeedbackActuator *actuator);
    QDeclarativeFeedbackActuator* actuator() const;
    QQmlListProperty<QDeclarativeFeedbackActuator> availableActuators();

#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
    #define qqmlistproperty_size_t qsizetype
#else
    #define qqmlistproperty_size_t int
#endif

    static qqmlistproperty_size_t actuator_count(QQmlListProperty<QDeclarativeFeedbackActuator> *prop);
    static QDeclarativeFeedbackActuator *actuator_at(QQmlListProperty<QDeclarativeFeedbackActuator> *prop, qqmlistproperty_size_t index);

Q_SIGNALS:
    void intensityChanged();
    void attackTimeChanged();
    void attackIntensityChanged();
    void fadeTimeChanged();
    void fadeIntensityChanged();
    void periodChanged();
    void actuatorChanged();

private:
    QFeedbackHapticsEffect* d;
    QList<QDeclarativeFeedbackActuator*> m_actuators;
    QDeclarativeFeedbackActuator* m_actuator;
};

QML_DECLARE_TYPE(QDeclarativeHapticsEffect)

#endif // QDECLARATIVEHAPTICSEFFECT_P_H
