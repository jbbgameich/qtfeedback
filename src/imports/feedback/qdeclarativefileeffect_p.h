/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Feedback framework.  This header file may change from version
// to version without notice, or even be removed.
//
// We mean it.
//
//

#ifndef QDECLARATIVEFILEEFFECT_P_H
#define QDECLARATIVEFILEEFFECT_P_H

#include "qdeclarativefeedbackeffect_p.h"

QT_USE_NAMESPACE

class QDeclarativeFileEffect : public QDeclarativeFeedbackEffect
{
    Q_OBJECT

    Q_PROPERTY(bool loaded READ isLoaded WRITE setLoaded NOTIFY loadedChanged)
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QStringList supportedMimeTypes READ supportedMimeTypes CONSTANT)

public:
    explicit QDeclarativeFileEffect(QObject *parent = nullptr);
    bool isLoaded() const;
    void setLoaded(bool v);
    QUrl source() const;
    void setSource(const QUrl & url);
    QStringList supportedMimeTypes();

Q_SIGNALS:
    void loadedChanged();
    void sourceChanged();

public Q_SLOTS:
    void load();
    void unload();

private:
    QFeedbackFileEffect* d;
};

QML_DECLARE_TYPE(QDeclarativeFileEffect)

#endif // QDECLARATIVEFILEEFFECT_P_H
