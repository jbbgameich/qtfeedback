/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

#include "qdeclarativefileeffect_p.h"
/*!
    \internal
    \qmltype FileEffect
    \brief The FileEffect element represents feedback data stored in a file.
    \ingroup qml-feedback-api
    \inherits FeedbackEffect

    \snippet doc/src/snippets/declarative/declarative-feedback.qml File Effect

    \sa HapticsEffect, {QFeedbackActuator}
*/
QDeclarativeFileEffect::QDeclarativeFileEffect(QObject *parent)
    : QDeclarativeFeedbackEffect(parent)
{
    d = new QFeedbackFileEffect(this);
    setFeedbackEffect(d);
}

/*!
  \qmlproperty bool FileEffect::loaded

  This property is true if this feedback effect is loaded.
  */
bool QDeclarativeFileEffect::isLoaded() const
{
    return d->isLoaded();
}
void QDeclarativeFileEffect::setLoaded(bool v)
{
    if (v != d->isLoaded()) {
        d->setLoaded(v);
        Q_EMIT loadedChanged();
    }
}

/*!
  \qmlproperty url FileEffect::source

  This property stores the URL for the feedback data.
  */
QUrl QDeclarativeFileEffect::source() const
{
    return d->source();
}
void QDeclarativeFileEffect::setSource(const QUrl & url)
{
    if (url != d->source()) {
        d->setSource(url);
        Q_EMIT sourceChanged();
    }
}

/*!
  \qmlproperty list<string> FileEffect::supportedMimeTypes

  This property holds the MIME types supported for playing effects from a file.
  */
QStringList QDeclarativeFileEffect::supportedMimeTypes()
{
    return d->supportedMimeTypes();
}

/*!
    \qmlmethod  FileEffect::load()

    Makes sure that the file associated with the feedback object is loaded.
    \sa QFeedbackFileEffect::load()
*/
void QDeclarativeFileEffect::load()
{
    if (!isLoaded()) {
        d->load();
        Q_EMIT loadedChanged();
    }
}

/*!
    \qmlmethod  FileEffect::unload()

    Makes sure that the file associated with the feedback object is unloaded.
    \sa QFeedbackFileEffect::unload()
*/
void QDeclarativeFileEffect::unload()
{
    if (isLoaded()) {
        d->unload();
        Q_EMIT loadedChanged();
    }
}
